multiArrayVsn = function(aFrame){
  caster = formula("array_id + rowSeq ~ droplevels(combine)")
  X   = acast(aFrame, caster, value.var = "value"   )
  rs 	= acast(aFrame, caster, value.var = "rowSeq"   )
  cs 	= acast(aFrame, caster, value.var = "colSeq"   )
  bNa = is.na(apply(X,1,sum))
  H = tryCatch({
    aVsn = vsn2(X[!bNa,], verbose = FALSE)
    predict(aVsn, newdata = X)
  },
  error = function(e){
    print(e)
    return(NULL)
  })
  if(!is.null(H)){
    aResult = data.frame(rowSeq = as.vector(rs), colSeq = as.vector(cs), H = as.vector(H) )
    aResult = aResult[!is.na(aResult$H),]
    aResultTable = data.table(vsnFrame = list(aResult), vsnModel = list(aVsn))
  } else {
    aResultTable = data.table(NULL)
  }
  return(aResultTable)
}
